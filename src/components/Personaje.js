import React, {useState, useEffect, Fragment} from 'react';
import axios from 'axios';
import Spinner from './Spinner'

const Personaje = ({personaje, showAlertAddFavorito,showAlertRemoveFavorito}) => {
    const [infoPersonaje, actualizarInfoPersonaje] = useState({});
    const [participaciones, actualizarParticipaciones] = useState([])
    const [especie, actualizarEspecie] = useState({});
    const [hogarNacimiento, actualizarHogarNacimiento] = useState({});
    const [isFav, actulizarIsFav] = useState(null);
    const [loading, actualizarLoading] = useState(true);
   
    useEffect(()=>{
        getPersonaje();
        // eslint-disable-next-line 
    },[])

    const getPersonaje = async () =>{
        const resultado = await axios.get(personaje);
        if(resultado.status === 200){
            actualizarInfoPersonaje(resultado.data);
            let existeItem = false
                let dataStorage = JSON.parse(localStorage.getItem('favoritos'));
                if(dataStorage){
                    dataStorage.map(fav => {
                        if(fav.infoPersonaje.name === resultado.data.name)
                            return existeItem = true;
                        return fav;
                    });
            actulizarIsFav(existeItem);
        }
            getFilms(resultado.data.films);
            getSpecie(resultado.data.species[0]);
            getHomeworld(resultado.data.homeworld);

        }
    }

    const getFilms = async (dataFilms) =>{
        const aux = []
        await dataFilms.map( async film =>{
            const resultado = await axios.get(film);
            if(resultado.status === 200){
                aux.push(resultado.data)
            }
        })

        actualizarParticipaciones(aux);
        setTimeout(() => {
            actualizarLoading(false);
          }, 3000);
    }

    const getSpecie = async (dataSpecie) =>{
        if(dataSpecie){
            const resultado = await axios.get(dataSpecie);
            if(resultado.status === 200){
                actualizarEspecie(resultado.data);
            }
        }
    }

    const getHomeworld = async (dataHome) =>{
        if(dataHome){
            const resultado = await axios.get(dataHome);
            if(resultado.status === 200){
                actualizarHogarNacimiento(resultado.data);
            }
        }
    }

    const addFavoritos = () =>{
        let existeItem = false
        let dataStorage = JSON.parse(localStorage.getItem('favoritos'));
        if(dataStorage){
            dataStorage.map(fav => {
                if(fav.infoPersonaje.name === infoPersonaje.name)
                    return existeItem = true;
                return fav;
            });
            if(existeItem){
                console.log('ya existe ')
            }else{
                dataStorage.push({
                    infoPersonaje,
                    participaciones,
                    especie,
                    hogarNacimiento
                });
                localStorage.setItem('favoritos',JSON.stringify(
                    dataStorage
                ))
            }
        }else{
            localStorage.setItem('favoritos',JSON.stringify([{
                infoPersonaje,
                participaciones,
                especie,
                hogarNacimiento
            }]))
        }
        actulizarIsFav(true);
        setTimeout(() => {
            showAlertAddFavorito(infoPersonaje.name);
        }, 500);
    }

    const removeFavorito = (name) =>{
        let dataStorage = JSON.parse(localStorage.getItem('favoritos'));
        let nuevosFavoritos = dataStorage.filter(fav =>{
            return fav.infoPersonaje.name !== name
        });

        actulizarIsFav(false);
        localStorage.setItem('favoritos',JSON.stringify(
            nuevosFavoritos
        ));
        setTimeout(() => {
            showAlertRemoveFavorito(infoPersonaje.name); 
        }, 500);
        
    }

    return ( 
        <div className="row">
            <div className="col-12 col-sm-12 col-md-7 col-lg-7 mx-auto p-2">
                <div className="card cardPersonaje">
                    {loading ? <Spinner />
                    :
                    <Fragment>
                        <div className="card-title namePersonaje">
                            {infoPersonaje.name}
                        </div>
                        <hr className="divider"></hr>
                        <div className="col-12 pl-4">
                            <p className="textTipo">species: <span className="textDetalle">{Object.keys(especie).length !== 0 ? especie.name.toUpperCase() :null}</span></p>
                            <p className="textTipo">homeworld: <span className="textDetalle">{Object.keys(hogarNacimiento).length !== 0 ? hogarNacimiento.name.toUpperCase() : null}</span></p>
                            <p className="textTipo">population: <span className="textDetalle">{Object.keys(hogarNacimiento).length !== 0 ? hogarNacimiento.population : null}</span></p> 
                            <p className="mb-0 textTipo">appears in:</p>
                            {participaciones.length !== 0?
                                participaciones.map(participacion =>(
                                    <p key={participacion.episode_id} className="textParticipaciones"> - {participacion.title.toUpperCase()}</p>
                                ))
                            :null}
                        </div>
                        <div className="card-body text-center">
                            {isFav ? 
                                <button className="btn mx-auto btnRemoveFavorito" onClick={() => removeFavorito(infoPersonaje.name)}>
                                    Remove favorite
                                </button>
                            :
                                <button className="btn mx-auto btnFavoritos" onClick={() => addFavoritos()}>
                                    Add to favorites
                                </button>
                            }
                            
                        </div>
                    </Fragment>
                    }
                </div>
            </div>
            
        </div>
     );
}
 
export default Personaje;
    
//#337986 #243a5a  #2b4268  #1d324e
//#070808