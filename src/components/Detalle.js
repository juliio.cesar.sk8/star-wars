import React, {useState, useEffect, Fragment} from 'react';
import Personaje from './Personaje';
import Header  from './Header';

const Listado = (props) => {
    const [infoPelicula, actualizarInfoPelicula] = useState({});
    const [alertaAdd, actualizarAlertaAdd] = useState({
        mostrar: false,
        name: 'addd'
    });
    const [alertaRemove, actualizarAlertaRemove]= useState({
        mostrar: false,
        name: 'holaa'
    });

    useEffect(()=>{
        const {pelicula} = props.location.state
        pelicula.title = pelicula.title.toUpperCase()
        actualizarInfoPelicula(pelicula);
        // eslint-disable-next-line 
    },[])

    const showAlertAddFavorito = (name) =>{
        actualizarAlertaAdd({
            mostrar: true,
            name
        });

        setTimeout(() => {
            actualizarAlertaAdd({
                mostrar: false,
                name: ''
            })
        }, 1500);
    }

    const showAlertRemoveFavorito = (name) =>{
        actualizarAlertaRemove({
            mostrar: true,
            name
        });

        setTimeout(() => {
            actualizarAlertaRemove({
                mostrar: false,
                name: ''
            })
        }, 1500);
    }

    return ( 
        <Fragment>
            <Header
                showHome
                showFav
            />
            <div className="row containerDetalle">
                <div className="col-12 col-sm-12 col-md-5 col-lg-5 resumenEpisodio">
                    <span className="titleEpisodio"><p>{infoPelicula.episode_id+ ': '+ infoPelicula.title}</p></span>
                    <p>{infoPelicula.opening_crawl}</p>
                </div>
                <div className="col-12 col-sm-12 col-md-6 col-lg-6 p-0 containerPersonaje">
                    {Object.keys(infoPelicula).length !== 0 ?
                        infoPelicula.characters.map( (personaje, index) => (
                            <Personaje 
                                key={index}
                                personaje = {personaje}
                                showAlertAddFavorito = {showAlertAddFavorito}
                                showAlertRemoveFavorito = {showAlertRemoveFavorito}
                            />
                        )):null
                    }
                </div>
            </div>
            {alertaAdd.mostrar ? 
                <div className="alertaAdd">{alertaAdd.name} added to favourites</div>
            :null}
            {alertaRemove.mostrar?
                <div className="alertaRemove"> {alertaRemove.name} is no longer part of your favourites</div>
            :null}
        </Fragment>
     );
}
 
export default Listado;