import React from 'react';
import Listado  from './components/Listado';
import Detalle  from './components/Detalle';
import Favoritos  from './components/Favoritos';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

function App() {
  return (
    <Router>
        <Switch>
          <Route exact path="/" component={Listado} />
          <Route exact path="/film/:id" component={Detalle} />
          <Route exact path="/favoritos" component={Favoritos} />
        </Switch>
    </Router>
  );
}

export default App;